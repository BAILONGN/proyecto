package bailongabriel.facci.faccicomunidades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class menuprincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuprincipal);
    }

    public void talleres(View view) {
        Intent talleres = new Intent(this, tallerActivity.class);
        startActivity(talleres);
    }

    public void eventos(View view) {
        Intent eventos = new Intent(this, actividades.class);
        startActivity(eventos);
    }

    public void beneficios(View view) {
        Intent beneficios = new Intent(this, ayudas.class);
        startActivity(beneficios);
    }

    public void concursos(View view) {
        Intent concursos = new Intent(this, menuprincipal.class);
        startActivity(concursos);
    }

    public void programacionweb(View view) {
        Intent programacionweb = new Intent(this, tallerActivity.class);
        startActivity(programacionweb);
    }

    public void robotica(View view) {
        Intent robotica = new Intent(this, tallerActivity.class);
        startActivity(robotica);
    }

    public void realidadvirtual(View view) {
        Intent realidadvirtual = new Intent(this, tallerActivity.class);
        startActivity(realidadvirtual);
    }

    public void t_mobile(View view) {
        Intent t_mobile = new Intent(this, actividades.class);
        startActivity(t_mobile);
    }

    public void ieee(View view) {
        Intent ieee = new Intent(this, actividades.class);
        startActivity(ieee);
    }

    public void microsoft(View view) {
        Intent microsoft = new Intent(this, actividades.class);
        startActivity(microsoft);
    }

    public void otros(View view) {
        Intent otros = new Intent(this, actividades.class);
        startActivity(otros);
    }

    public void diplomas(View view) {
        Intent diplomas = new Intent(this, ayudas.class);
        startActivity(diplomas);
    }

    public void becas(View view) {
        Intent becas = new Intent(this, ayudas.class);
        startActivity(becas);
    }

    public void certificados(View view) {
        Intent certificados = new Intent(this, ayudas.class);
        startActivity(certificados);
    }

    public void nacionales(View view) {
        Intent nacionales = new Intent(this, concursos.class);
        startActivity(nacionales);
    }

    public void internacionales(View view) {
        Intent internacionales = new Intent(this, concursos.class);
        startActivity(internacionales);
    }
}


