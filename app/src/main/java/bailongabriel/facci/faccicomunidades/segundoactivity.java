package bailongabriel.facci.faccicomunidades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class segundoactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundoactivity);
    }

    public void registrarse(View view) {
        Intent registrarse = new Intent(this, MainActivity.class);
        startActivity(registrarse);
    }

    public void regresar(View view) {

        Intent regresar = new Intent(this, MainActivity.class);
        startActivity(regresar);
    }
}
