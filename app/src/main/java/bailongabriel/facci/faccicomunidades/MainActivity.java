package bailongabriel.facci.faccicomunidades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void registro(View view) {

        Intent registro = new Intent(this, segundoactivity.class);
        startActivity(registro);
    }

    public void login(View view) {

        Intent login = new Intent(this, terceractivity.class);
        startActivity(login);

    }
}